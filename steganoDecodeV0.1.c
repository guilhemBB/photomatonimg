#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#define TAILLE 256
void lireCouleur(FILE* fichier, int tab[TAILLE][TAILLE][3]){
  for(int ii=0;ii<3;ii++){
  while (fgetc(fichier)!='\n') {}
  }

  for(int i=0;i<TAILLE;i++){
    for(int j=0;j<TAILLE;j++){
      for(int col=0; col<3; col++)
      tab[i][j][col]=fgetc(fichier);
    }
  }


}
void ecrireEnCouleur(FILE* fichier, int tab[TAILLE][TAILLE][3]){
  char* PREF= "P6\n256 256\n255\n";
  fprintf(fichier, PREF);
  for(int i=0;i<TAILLE;i++){
    for(int j=0;j<TAILLE;j++){
      for(int col=0;col<3;col++){
      fputc(tab[i][j][col],fichier);
    }
    }
  }
  fputc('\n',fichier);

}
void steganoDecode(int coffre[TAILLE][TAILLE][3], int secretExtrait[TAILLE][TAILLE][3]){
  for(int i=0;i<TAILLE;i++){
    for(int j=0;j<TAILLE;j++){
      for(int col=0;col<3;col++){
        secretExtrait[i][j][col]=(coffre[i][j][col]%16)*16;}
    }
  }
}

int main(int argc, char *argv[]){
  FILE* fichier = fopen(argv[1], "r");
  FILE* fichierDest = fopen(argv[2], "w");

  int imageEnCouleur[TAILLE][TAILLE][3];
  int imageEnCouleurSecret[TAILLE][TAILLE][3];
  int imageEnCouleurDEST[TAILLE][TAILLE][3];

  lireCouleur(fichier,imageEnCouleur);
  steganoDecode(imageEnCouleur,imageEnCouleurDEST);

  ecrireEnCouleur(fichierDest, imageEnCouleurDEST);

  return 0;
}
