#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#define TAILLE 256

void lire(FILE* fichier, int tab[TAILLE][TAILLE]){


  for(int ii=0;ii<3;ii++){
  while (fgetc(fichier)!='\n') {}
  }

  for(int i=0;i<TAILLE;i++){
    for(int j=0;j<TAILLE;j++){
      tab[i][j]=fgetc(fichier);
    }
  }
}

void lireCouleur(FILE* fichier, int tab[TAILLE][TAILLE][3]){
  for(int ii=0;ii<3;ii++){
  while (fgetc(fichier)!='\n') {}
  }

  for(int i=0;i<TAILLE;i++){
    for(int j=0;j<TAILLE;j++){
      for(int col=0; col<3; col++)
      tab[i][j][col]=fgetc(fichier);
    }
  }


}

void extraireCouleur(int tab[TAILLE][TAILLE][3], int compoRouge[TAILLE][TAILLE], int compoVert[TAILLE][TAILLE], int compoBleu[TAILLE][TAILLE]){
  for(int i=0; i<TAILLE; i++){
    for(int j=0; j<TAILLE; j++){
      compoRouge[i][j]=tab[i][j][0];
      compoVert[i][j]=tab[i][j][1];
      compoBleu[i][j]=tab[i][j][2];
    }
  }
}

void combineCouleur(int tab[TAILLE][TAILLE][3], int compoRouge[TAILLE][TAILLE], int compoVert[TAILLE][TAILLE], int compoBleu[TAILLE][TAILLE]){
  for(int i=0; i<TAILLE; i++){
    for(int j=0; j<TAILLE; j++){
      tab[i][j][0]=compoRouge[i][j];
      tab[i][j][1]=compoVert[i][j];
      tab[i][j][2]=compoBleu[i][j];
    }
  }


}

int MyOpp(int nb,int mod){
  int res=(2*nb)%(mod-1);
  if (nb==mod-1) res=nb;
  return res;
}

void photomaton(int tab1[TAILLE][TAILLE], int tab2[TAILLE][TAILLE]){
  for(int i=0;i<TAILLE-1;i++){
      for(int j=0;j<TAILLE-1;j++){
        tab2[i][j]=tab1[MyOpp(i,TAILLE)][MyOpp(j,TAILLE)];
      }
    }
}

void photomatonEngroupe(int tab1[TAILLE][TAILLE], int tab2[TAILLE][TAILLE], int taille_groupe){

  //int taille_groupe=32;
  int nbgroupe = TAILLE/taille_groupe;
  int i_de_groupe;
  int j_de_groupe;
  int i_dans_groupe;
  int j_dans_groupe;

  for(int i=0;i<TAILLE-1;i++){
      for(int j=0;j<TAILLE-1;j++){

        i_de_groupe = i/taille_groupe;
        j_de_groupe = j/taille_groupe;
        i_dans_groupe = i%taille_groupe;
        j_dans_groupe = j%taille_groupe;

        tab2[i][j]=tab1[MyOpp(i_de_groupe,nbgroupe)*taille_groupe+i_dans_groupe][MyOpp(j_de_groupe,nbgroupe)*taille_groupe+j_dans_groupe];

      }
    }
}

void photomatonMoy(int tab1[TAILLE][TAILLE], int tab2[TAILLE][TAILLE]){
  for(int i=0;i<TAILLE-1;i++){
      for(int j=0;j<TAILLE-1;j++){
        tab2[i][j]=tab1[MyOpp(i,TAILLE)][MyOpp(j,TAILLE)]+tab1[MyOpp(i,TAILLE)][MyOpp(j+1,TAILLE)]+tab1[MyOpp(i+1,TAILLE)][MyOpp(j,TAILLE)]+tab1[MyOpp(i+1,TAILLE)][MyOpp(j+1,TAILLE)]/8;

      }
    }

}

void diag(int tab1[TAILLE][TAILLE], int tab2[TAILLE][TAILLE]){

  for(int i=0;i<TAILLE;i++){
      for(int j=0;j<TAILLE;j++){
        tab2[i][j]=tab1[(i+64)%(TAILLE)][(j+32)%(TAILLE)];
      }
    }

}

void myshred(int tab1[TAILLE][TAILLE], int tab2[TAILLE][TAILLE]){
  int suiv;
  int prec;
  for(int i=0;i<TAILLE;i++){
      for(int j=0;j<TAILLE;j++){
        prec=j-1;
        suiv=j+1;
        if(suiv==256)suiv=0;
        if(prec==-1)prec=255;


        if(i%2==0)
          tab2[i][j]=tab1[i][prec];
        else
          tab2[i][j]=tab1[i][suiv];
      }
    }
}

void miroir(int tab1[TAILLE][TAILLE], int tab2[TAILLE][TAILLE]){

  for(int i=0;i<TAILLE;i++){
      for(int j=0;j<TAILLE;j++){

        tab2[i][j]=tab1[j][i];
      }
    }
}

void ecrire(FILE* fichier, int tab[TAILLE][TAILLE]){
  char* PREF= "P5\n256 256\n255\n";
  fprintf(fichier, PREF);
  for(int i=0;i<TAILLE;i++){
    for(int j=0;j<TAILLE;j++){
      fputc(tab[i][j],fichier);
    }
  }
  fputc('\n',fichier);

}

void ecrireEnCouleur(FILE* fichier, int tab[TAILLE][TAILLE][3]){
  char* PREF= "P6\n256 256\n255\n";
  fprintf(fichier, PREF);
  for(int i=0;i<TAILLE;i++){
    for(int j=0;j<TAILLE;j++){
      for(int col=0;col<3;col++){
      fputc(tab[i][j][col],fichier);
    }
    }
  }
  fputc('\n',fichier);

}


int main(int argc, char *argv[]){
  FILE* fichier = fopen(argv[1], "r");
  FILE* fichierDest = fopen(argv[2], "w");

  int tab[TAILLE][TAILLE];
  int tabDest[TAILLE][TAILLE];
  int la3[TAILLE][TAILLE];

  int imageEnCouleur[TAILLE][TAILLE][3];
  int imageEnCouleurDEST[TAILLE][TAILLE][3];

  int compoRouge[TAILLE][TAILLE];
  int compoVert[TAILLE][TAILLE];
  int compoBleu[TAILLE][TAILLE];

  int compoRougeDD[TAILLE][TAILLE];
  int compoVertDD[TAILLE][TAILLE];
  int compoBleuDD[TAILLE][TAILLE];

  //lire(fichier, tab);
  lireCouleur(fichier,imageEnCouleur);
  extraireCouleur(imageEnCouleur,compoRouge, compoVert, compoBleu);
  photomatonEngroupe(compoRouge, compoRougeDD,32);
  photomatonEngroupe(compoVert, compoVertDD,32);
  photomatonEngroupe(compoBleu, compoBleuDD,32);
  combineCouleur(imageEnCouleurDEST,compoRougeDD,compoVertDD,compoBleuDD);
  for(int jj=0;jj<1;jj++){
    //photomatonEngroupe(tab,tabDest,2);
    //printf("totoyman\n" );
  }

  ecrireEnCouleur(fichierDest, imageEnCouleurDEST);

  return 0;
}
